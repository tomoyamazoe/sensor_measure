# -*- coding: utf-8 -*-
import os
from os.path import join, dirname
from dotenv import load_dotenv
from datetime import datetime
import urllib.request
import base64, json

load_dotenv(verbose=True)
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

# sensor id to be posted measures.
sensor_id = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'

# debug_mode True: get sensor measure, False: post sensor measure
#debug_mode = False
debug_mode = True

# Auth access token
access_token = ''
token_file = '.auth_token.json'

def is_auth_valid():
    # load from file
    try:
        fd = open(token_file, 'r')
        tokens = json.load(fd)
        fd.close()
        expires_in = tokens['expires_in']
        granted = datetime.fromtimestamp(tokens['granted'])
        now = datetime.now()
        delta = now - granted
        print("granted {0}, now {1}, delta {2}".format(granted, now, delta))
        if delta.total_seconds() < int(expires_in):
            global access_token
            access_token = tokens['access_token']
            print("access token valid")
            return True
        else:
            print("token expired")
    except Exception as e:
        print(e)
        print("failed to get token from file")
    return False

def update_auth():
    print("getting auth token ...")
    user = os.getenv('AUTH0_USERNAME')
    password = os.getenv('AUTH0_PASSWORD')
    print(user)
    # print(password)
    header = { 'Content-Type': 'application/x-www-form-urlencoded' }
    url = 'https://api.senseye.io/oauth/token'
    data = {
        'username': user,
        'password': password,
        'grant_type': 'password'
    }
    try:
        params = urllib.parse.urlencode(data).encode('utf-8')
        request = urllib.request.Request(url=url, headers=header, data=params)
        response = urllib.request.urlopen(request)
        response_code = response.getcode()
        print("response {}".format(response_code))
        if response_code != 200:
            print("Failed response exit")
            return False
        body = json.loads(response.read().decode('utf-8'))
        global access_token
        access_token = body['access_token']
        expires_in = body['expires_in']
        # save token to file
        tokens = {}
        tokens['access_token'] = access_token
        tokens['expires_in'] = expires_in
        tokens['granted'] = datetime.now().timestamp()
        fd = open(token_file, 'w')
        json.dump(tokens, fd)
        fd.close()
        print("auth updated")
        return True
    except Exception as e:
        print(e)
    print("Failed to update auth")
    return False

def get_measure():
    print('\nget_measure()')
    header = { 'Authorization': 'Bearer ' + access_token }
    url = 'https://api.senseye.io/v1/hub/sensors/{0}/data'.format(sensor_id)
    request = urllib.request.Request(url=url, headers=header)
    response = urllib.request.urlopen(request)
    response_code = response.getcode()
    print("response {}".format(response_code))
    if response_code != 200:
        print("Failed response exit")
        #return False
    data = json.loads(response.read().decode('utf-8'))
    print('-------------------')
    print(data)
    print(json.dumps(data, indent=2))
    print('-------------------')

def post_measure():
    print('\npost_measure()')
    header = { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + access_token }
    url = 'https://api.senseye.io/v1/hub/sensors/{0}/data'.format(sensor_id)
    json_data = generate_post_body()
    print(json_data)
    data = json.dumps(json_data).encode('utf-8')
    request = urllib.request.Request(url=url, headers=header, data=data)
    response = urllib.request.urlopen(request)
    response_code = response.getcode()
    if response_code == 201:
        print("data successfuly posted response: {}".format(response_code))
    else:
        print("failed to post response: {}".format(response_code))
        data = json.loads(response.read().decode('utf-8'))
        print(data)

def generate_post_body():
    now = datetime.now()
    print(now)
    timestamp = int(now.timestamp())
    measure_1 = 12.34  # dummy data
    measure_2 = 100    # dummy data
    measures = []
    measures.append({"n": "measure_1", "v": measure_1, "t": timestamp})
    measures.append({"n": "measure_2", "v": measure_2, "t": timestamp})
    body = {
        'e': measures
    }
    return body


if __name__ == "__main__":

    if not is_auth_valid():
        update_auth()
    if debug_mode:
        get_measure()
    else:
        post_measure()

