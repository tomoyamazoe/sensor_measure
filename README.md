# README #

Sample script to GET/POST sensor measure to the application.

## get_post/post_sensor_measure.py
POST/GET sensor measure to the app via API.

Configure your sensor id in the script.
```
sensor_id = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
```
two sample measures in generate_post_body().

debug_mode:
- True: get measure
- False: post measure
```
debug_mode = True
```

### authentificaton
auth token is saved in local file and used it until expiration.

## .env file
Create `.env` file in the same directory. Put username and password.
```
AUTH0_USERNAME="xxxxxxxxxxxx"
AUTH0_PASSWORD="xxxxxxxxxxxx"
```

